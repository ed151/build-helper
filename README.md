*Note: To use this build helper, most people simply need to add the following to their `.gitlab-ci.yml` file*

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:alpine3.12
```

After adding this, you'll have `git`, `helm`, `kubectl`, `vault`, `crane`, and other tools.
See [Dockerfile](./Dockerfile) for specifics on what is installed.

## Alpine Version

A version of this is image is available based on alpine linux.  The goal of the alpine
version is to shrink the image size in order to give a slight boost to the setup time
of your gitlab-ci jobs.

To use the alpine version, add the following to your .gitlab-ci.yml:

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:alpine3.12
```

#### Docker registry cleanup

The alpine image also includes the [cleanup-docker-image](cleanup-docker-images.md) scripts

#### oc wrapper
Due to the size of the `oc` binary, it has been dropped in favor of a wrapper script around `kubectl`.

The `oc` wrapper supports the following calls:
```bash
oc login ${K8S_URL} --token=<token>
```
```bash
oc project ${NAMESPACE_NAME}
```

All other calls to `oc` will be passed onto `kubectl` with the same arguments


## Ubuntu Version

This is the original version of this image.  It contains a base ubuntu images as well
as extra tools like `git`, `helm`, `oc`, `kubectl`, and `vault`.

To use this version, add the following to your .gitlab-ci.yml:

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:ubuntu18
```

You can also use the legacy image name:

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:master
```
